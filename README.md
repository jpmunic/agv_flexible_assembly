### README ###

In order to run the webserver, you need to have node.js installed.
Instructions for installing node.js can be found [here](http://www.hostingadvice.com/how-to/install-nodejs-ubuntu-14-04/#node-version-manager).

To edit the directions to get to each station, edit database/directions.csv
Instructions for adding directions to the directions.csv file:
	The first column is the name of the station
		Station names may not contain spaces
	The next columns are the directions to get to that station from the base station

For questions, e-mail jpmunic@gmail.com
