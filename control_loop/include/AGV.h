#ifndef AGV_H
#define AGV_H

#include <vector>
#include <Ultrasonic_sensor.h>
#include <Conveyer.h>
#include <Magnetic_tape_sensor.h>
#include <Wheel_motor_controller_interface.h>

class AGV{
    public:
	void follow_directions(int conveyer_id,const queue<string> & directions);
	void return_home();
	void deliver_bin(int conveyer_id,const string & direction){
    private:
	std::vector<Ultrasonic_sensor> ultrasonic_sensors;
	std::vector<Conveyer> conveyers;
	Wheel_motor_controller_interface wheel_motor_controller_interface;
};
#endif
