#ifndef CONVEYERS_H
#define CONVEYERS_H

class Conveyers{
    public:
	//enums
	enum Direction{
		LEFT,
		RIGHT
	}
	//functions
	Conveyer(int input_id);
	void deliver_box(Direction direction);
	void recieve_box(Direction direction);
	
    private:
	//functions
	void raise_guard_rail();
	void lower_guard_rail();
	//variables
	Infrared_sensor left_infrared_sensor;
	Infrared_sensor right_infrared_sensor;
	int id;
};

#endif
