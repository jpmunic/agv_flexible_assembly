#ifndef DATABASE_INTERFACE_H
#define DATABASE_INTERFACE_H

#include <queue> //for directions

class Database_interface{
    public:
	int get_target_station();
	std::queue<string> get_directions(const int target_station);
}

#endif
