#ifndef PARAMETERS_H
#define PARAMETERS_H

//define the Parameters class
class Parameters{
    public:
	//functions
	Parameters();
	float get_move_speed();
	float get_collision_distance();
    private:
	//functions
	void get_parameters_from_file()
	//variables
	float move_speed;
	float collision_distance;
};

//create a global instance of Parameters
Parameters parameters;

#endif
