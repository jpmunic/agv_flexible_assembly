#ifndef ULTRASONIC_SENSOR_H
#define ULTRASONIC_SENSOR_H

class Ultrasonic_sensor{
    public:
	enum Position{
		LEFT,
		MIDDLE,
		RIGHT
	}
	Ultrasonic_sensor(Position);
	float check_distance();
    private:
	unsigned int signal_pin; //according to wiringPi
};

#endif
