#include "AGV.h"
#include <iostream>
#include <queue> //for the queue of directions
#include "Parameters.h"
#include "error_protocol.h"

AGV::AGV(){
	//add the three ultrasonic sensors to the vector
	ultrasonic_sensors.push_back(Ultrasonic_sensor(Ultrasonic_sensor::LEFT));
	ultrasonic_sensors.push_back(Ultrasonic_sensor(Ultrasonic_sensor::MIDDLE));
	ultrasonic_sensors.push_back(Ultrasonic_sensor(Ultrasonic_sensor::RIGHT));
}

void AGV::follow_directions(const int conveyer_id, const queue<string> & directions){
	while (true){ 
		//check safety
		for (int i=0;i<ultrasonic_sensors.size();i++){
			float collision_distance=parameters.get_collision_distance(); 
			if (ultrasonic_sensors[i].check_distance()<collision_distance){
				fatal_error();
			}
		}

		string direction=directions.front();

		// Check if it's done all the instructions
		if (direction=="END"){
			motors.stop();
			directions.pop();
			break;
		}
		
		// Check if the AGV is on a station
		if (magnetic_tape_sensor.is_on_station()){
			if (direction.substr(0,7)=="DELIVER"){
				wheel_motor_controller_interface.stop();
				deliver_box(conveyer_id,direction);
				magnetic_tape_sensor.ignore_stations_for_awhile();
				directions.pop();
			}
			else if (direction=="IGNORE STATION"){
				magnetic_tape_sensor.ignore_stations_for_awhile();
				directions.pop();
			}
		}

		//check for fork in road
		if (magnetic_tape_sensor.is_on_fork()){
			if (directions.front()=="LEFT"){
				go_left_on_fork();
				directions.pop();
			}
			else if (directions.front()=="RIGHT"){
				go_right_on_fork();
				directions.pop();
			}
		}

		//zeroth index in velocity_vector is angle in radians, first index is magnitude in mph
		double velocity_vector[2];
		velocity_vector=magnetic_tape_sensor.calculate_velocity();

		motors.change_speed(velocity_vector)
	}
}

void AGV::return_home(){
	//not implemented	
}

void AGV::deliver_bin(int conveyer_id,const string & direction){
	//not implemented
}
