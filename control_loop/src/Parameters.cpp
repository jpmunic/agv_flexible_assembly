#include "Parameters.h"
#include <fstream>

Parameters::Parameters(){
	get_parameters_from_file();
}

Parameters::get_move_speed(){
	return move_speed;
}

Parameters::get_min_collision_distance(){
	return collision_distance
}

Parameters::get_parameters_from_file(){
	std::ifstream parameters_file;
	parameters_file.open("../database/parameters");
	if (!parameters_file.good()){
		std::cout<<"couldn't open file!"<<std::endl;
		return;
	}
	
	int maximum_characters_per_line=150;
	std::vector<std::string> input_parameters;
	while (!parameters_file.eof()){
		char buf[maximum_characters_per_line];
		parameters_file.getline(buf,maximum_characters_per_line);

		//skip if this line is a comment
		if (buf[0]=='#')
			continue;
		
		//get the input parameters in a vector
		std::string current_line=buf;
		for (int i=0;i<current_line.size();i++){
			if (current_line[i]=='='){
				input_parameters.push_back(current_line.substr(i+1,current_line.size()));
				break;
			}
		}
	}

	if (input_parameters.size()==2){
		move_speed=atof(input_parameters[0].c_str());

		collision_distance=atof(input_parameters[1].c_str());
	}
}
