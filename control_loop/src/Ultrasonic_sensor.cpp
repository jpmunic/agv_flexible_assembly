#include "Ultrasonic_sensor.h"
#include <wiringPi.h>

Ultrasonic_sensor::Ultrasonic_sensor(Position input_position): position(input_position){
	//set the signal pin based on the position
	switch (position){
		case LEFT:
			signal_pin=0; //GPIO17 on the diagram
			break;
		case MIDDLE:
			signal_pin=1; //GPIO18 on the diagram
			break;
		case RIGHT:
			signal_pin=2; //GPIO27 on the diagram
			break;
	}
}

float check_distance(){
	float distance; //in inches
	unsigned int start_time=micros(); //in microseconds
	unsigned int end_time=start_time;	

	wiringPiSetup(); //move this somewhere better!

	pinMode(signal_pin,OUTPUT);

	// Set the signal to low for 2 microseconds
	digitalWrite(signal_pin,LOW);
	delayMicroseconds(2);

	// Set the signal to high for 5 microseconds, then set it back to low.
	// This activates the ping
	digitalWrite(signal_pin,HIGH);
	delayMicroseconds(5);
	digitalWrite(signal_pin,LOW);
	
	// Set the pinmode to input now
	pinMode(signal_pin,INPUT);

	// Time the pulse	
	while (digitalRead(signal_pin)==LOW){
		start_time=micros();
	}
	while(digitalRead(signal_pin)==HIGH){
		; //do nothing
	}
	end_time=micros();

	if (end_time>start_time){
		unsigned int duration=end_time-start_time; //in microseconds
		distance=(float)duration/148;
	}
	else{
		// Micros rollover. Make the distance high.
		distance=99;
	}

	return distance;
}
