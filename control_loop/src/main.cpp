#include "AGV.h"
#include <queue> //for directions
#include <string>
#include "Database_interface"

int main(int argc, char *argv[]){
	AGV agv;
	Database_interface database;

	// Main control loop
	while(true){
		// Wait for there to be an instruction in the request queue
		while(database.request_queue.is_empty()){
			sleep(5000);//miliseconds
		}

		// Retrieve the instruction from the request queue
		Database_interface::Request * request  = &database.request_queue.front();
		string & target_station=request->station_name;
		int conveyer_id=request->conveyer_id;
		
		// Get the directions to the target station from the database
		std::queue<string> directions;
		directions=database.get_directions(target_station);
		
		// Tell the agv to go and deliver the bins
		agv.follow_instructions(conveyer_id,directions);

		// Remove the top request in the queue, since the agv delivered the bin
		database.remove_top_request();

		// If there's no more bins to deliver, return to the charging station
		if (database.request_queue.is_empty()){
			agv.return_home();
		}
	}

	return 0;
}
