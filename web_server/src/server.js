// to run: node server.js

var http = require("http"); //sending page to browser
var url = require('url');
var fs = require("fs"); //reading in text files


//create the server
http.createServer(function (request, response) {

  // Parse the request containing file name
  var url_suffix = url.parse(request.url).pathname; //includes the '/'

  // Print the name of the file for which request is made.
  console.log("Request for " + url_suffix + " received.");

  var html_to_send;

  var error404=false;

  switch(url_suffix){
    case '/':
      html_to_send=create_homepage();
      break;
    case "/agv_sent":
      query=url.parse(request.url).query;
      halves=query.split('&');
      quarters12=halves[0].split('=');
      conveyer_id=quarters12[1];
      quarters34=halves[1].split('=');
      station_name=quarters34[1];
      if (conveyer_id!="DEFAULT" && station_name!="DEFAULT"){
        add_direction_to_queue(conveyer_id,station_name);
      }
      html_to_send=create_homepage();
      break;
    case "/edit_directions":
      html_to_send=create_edit_directions_page();
      break;
    case "/clear_queue":
      clear_queue();
      html_to_send=create_homepage();
      break;
    default:
      error404=true;
      break;
  }

  if (!error404){
    var HTTP_ok=200;
    response.writeHead(HTTP_ok, {'Content-Type': 'text/html'});	
    // Write the content of the file to response body
    response.write(html_to_send);
  }
  else{
    var HTTP_not_found=404;
    response.writeHead(HTTP_not_found, {'Content-Type': 'text/html'});
  }

  response.end();
}).listen(8081);

function get_station_names(){
  var file = fs.readFileSync('../../database/directions.csv', 'utf8');
  var lines = file.split("\n");
  var station_names=new Array();
  for (line_index=0;line_index<lines.length;line_index++){
    if (lines[line_index].length>0){
      var split_line=lines[line_index].split(",");
      station_names.push(split_line[0]);
    }
  }
  return station_names;
}

function create_homepage(){

  station_names=get_station_names();

  var html="<html lang=\"en-US\">"+
  "<head>"+
  "  <title>AGV Control</title>"+
  "</head>"+
  "<body>"+
  "  Send the AGV to a station:"+
  "  <form onsubmit=\"return add_selections_to_url()\">"+
  "    <select id=\"conveyer_id_select\">"+
  "      <option value=\"DEFAULT\">select conveyer</option>"+
  "      <option value=\"1\">1</option>"+
  "      <option value=\"2\">2</option>"+
  "      <option value=\"3\">3</option>"+
  "    </select>"+
  "    <select id=\"station_name_select\">"+
  "      <option value=\"DEFAULT\">select station</option>";

  for (i=0;i<station_names.length;i++){
    html+="<option value=\""+station_names[i]+"\">"+station_names[i]+"</option>";
  }

  html+="</select>"+
  "    <input type = \"submit\" value = \"Add to queue\">"+
  "  </form>"+
  "  <script type=\"text/javascript\">"+
  "    function add_selections_to_url(){"+
  "      var conveyer_id=document.forms[0].elements[0].value;"+
  "      var station_name=document.forms[0].elements[1].value;"+
  "      window.location.assign(\"agv_sent?conyever_id=\"+conveyer_id+\"&station_name=\"+station_name);"+
  "      return false;"+
  "    }"+
  "  </script>"+
  "  <br>"+
  "  Delivery request queue:"+
  "  <table border=\"1\">"+
  "    <tr>"+
  "      <td>Conveyer</td>"+
  "      <td>Station</td>"+
  "    </tr>";
  
  //read in the request queue from the database and add the lines to the table
  var all_request_queue = fs.readFileSync('../../database/request_queue.csv', 'utf8');
  requests=all_request_queue.split('\n');
  for (request_index=0;request_index<requests.length-1;request_index++){
    parts=requests[request_index].split(',');
    conveyer_id=parts[0];
    station_name=parts[1];
    html+="<tr>";
    html+="  <td>"+conveyer_id+"</td>";
    html+="  <td>"+station_name+"</td>";
    html+="</tr>";
  }

  html+="</table>"+
  "  <br>"+
  "  <form action=\"clear_queue\">"+
  "    <input type=\"submit\" value=\"clear queue\"/>"+
  "  </form>"+
  "  <form action=\"/\">"+
  "    <input type=\"submit\" value=\"refresh\"/>"+
  "  </form>"+
  "</body>"+
  "</html>";
    
  return html;
}

function add_direction_to_queue(conveyer_id,station_name){
  var string_to_append=conveyer_id+","+station_name+"\n";
  fs.appendFileSync('../../database/request_queue.csv',string_to_append);
  return false;
}

function create_edit_directions_page(){
}

function clear_queue(){
  fs.writeFileSync('../../database/request_queue.csv', '');
  console.log("directions queue cleared!");
}

function create_404(url_suffix){

  var html="<html lang=\"en-US\">";
  html+="<body>";
  html+="  <p>404: "+url_suffix+" not found</p>";
  html+="</body>";
  html+="</html>";

  return html;
}

// Console will print the message
console.log('Server running at http://127.0.0.1:8081/');
